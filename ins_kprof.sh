#!/bin/bash

if [ ! -d kvm-kmod/include/linux ]; then
        echo "ERROR: improper configuration. Follow instructions in kvm-kmod README first."
        exit
fi

# remove modules
if [ -e /dev/kvm ]; then
	/sbin/rmmod kvm-intel
	/sbin/rmmod kvm
        /sbin/rmmod kprof
fi

# install modules
/sbin/insmod kprof/kprof.ko
/sbin/insmod kvm-kmod/x86/kvm.ko
/sbin/insmod kvm-kmod/x86/kvm-intel.ko
