# export LD_LIBRARY_PATH=/home/cuong/hprof/libPerfMon/build
# In Ubuntu, the following command is similar to what
# we used to set LD_LIBRARY_PATH
CUR_DIR=`pwd`
echo "$CUR_DIR/lib" | sudo tee /etc/ld.so.conf.d/libperf_mon_papi.conf > /dev/null
sudo ldconfig

export PERF_MON_PROFILING=1; \
export PERF_MON_OUTPUT_FILE="/tmp/counter1.tsv"; \
sudo taskset 0x00000002 qemu/x86_64-softmmu/qemu-system-x86_64 ../co7.img -enable-kvm -m 512 -nographic
