#!/bin/bash

CUR_DIR=`pwd`
BUILD_DIR=${CUR_DIR}/build

# source dirs
PAPI_DIR=${CUR_DIR}/papi-5.4.1
LIBPERF_DIR=${CUR_DIR}/libPerfMon
SMALL_LOOP_DIR=${CUR_DIR}/small_loop

# target built dirs
PAPI_BUILD_DIR=${BUILD_DIR}/papi
LIBPERFMON_BUILD_DIR=${BUILD_DIR}/libperfmon
SMALL_LOOP_BUILD_DIR=${BUILD_DIR}/small_loop

rm -rf ${BUILD_DIR}
mkdir ${BUILD_DIR}

#### PAPI ####
cd ${PAPI_DIR}/src
./configure --prefix=${PAPI_BUILD_DIR}
make && make install

#### libPerfMon ####
mkdir -p ${LIBPERFMON_BUILD_DIR}/Papi
cd ${LIBPERFMON_BUILD_DIR}/Papi
cmake -G "Unix Makefiles" ${LIBPERF_DIR}/src/Backends/Papi && make && make install

mkdir -p ${LIBPERFMON_BUILD_DIR}/S2E
cd ${LIBPERFMON_BUILD_DIR}/S2E
cmake -G "Unix Makefiles" ${LIBPERF_DIR}/src/Backends/S2E && make && make install

#### bad_small_loop ####
mkdir ${SMALL_LOOP_BUILD_DIR}
cd ${SMALL_LOOP_BUILD_DIR}
cmake -G "Unix Makefiles" ${SMALL_LOOP_DIR} && make
mv bad_small_loop ${BUILD_DIR}
