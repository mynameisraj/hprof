#!/bin/bash

CUR_DIR=`pwd`
BUILD_DIR=${CUR_DIR}/build

# source dirs
LIBPERF_DIR=${CUR_DIR}/libPerfMon
SMALL_LOOP_DIR=${CUR_DIR}/small_loop

# target built dirs
LIBPERFMON_BUILD_DIR=${BUILD_DIR}/libperfmon
SMALL_LOOP_BUILD_DIR=${BUILD_DIR}/small_loop

rm -rf ${BUILD_DIR}
mkdir ${BUILD_DIR}

#### libPerfMon ####
mkdir -p ${LIBPERFMON_BUILD_DIR}/S2E
cd ${LIBPERFMON_BUILD_DIR}/S2E
cmake -G "Unix Makefiles" ${LIBPERF_DIR}/src/Backends/S2E && make && make install

#### bad_small_loop ####
mkdir ${SMALL_LOOP_BUILD_DIR}
cd ${SMALL_LOOP_BUILD_DIR}
cmake -G "Unix Makefiles" ${SMALL_LOOP_DIR}/S2E && make
mv small_loop_s2e ${BUILD_DIR}
