#include <stdio.h>
#include <stdlib.h>

#ifdef S2E_MODE
#include "Backends/S2E/s2e_mode.h"
#endif

#ifdef PAPI_MODE
#include <papi.h>
#include "Backends/Papi/profiling_papi_mode.h"
#include "Backends/Papi/checking_papi_mode.h"
#endif

#include "perf_monitor.h"

MonCallTable call_table;

/*============= Interface =================*/
void mon_init(void) {

    char* profiling_env = getenv("PERF_MON_PROFILING");
    char* profiling_output = getenv("PERF_MON_OUTPUT_FILE");
    char* out_fname = NULL;

    if (profiling_output != NULL) {
        out_fname = profiling_output;
    } else {
        out_fname = COUNTER_OUTPUT_FILENAME;
    }

    if ((profiling_env != NULL) && (profiling_env[0] != '0')) {
      // Profling mode
      profiling_init(&call_table, out_fname);
    } else {
      // Checking mode
      checking_init(&call_table);
    }
}

void mon_block_begin(void) {
    call_table.begin_func();
}

void mon_block_end(void) {
    call_table.end_func();
}

void mon_stop(void) {
    call_table.stop_func();
}

void mon_set_exit_reason(long exit_reason) {
    call_table.set_exit_reason_func(exit_reason);
}
