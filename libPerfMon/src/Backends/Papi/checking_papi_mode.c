#include <stdio.h>
#include <stdlib.h>

#include <papi.h>
#include "Backends/Papi/papi_mode.h"
#include "Backends/Papi/checking_papi_mode.h"

/*============= Checking mode =================*/
void mon_block_begin_checking_mode(void) {
    printf("mon_block_begin_checking\n");
}

void mon_block_end_checking_mode(void) {
    printf("mon_block_end_checking\n");
}

void mon_stop_checking_mode(void) {
    printf("mon_stop_checking\n");
}

void set_exit_reason_checking(long exit_reason) {
    printf("set exit reason: %ld\n", exit_reason);
}

void checking_init(MonCallTable *call_table) {
    // Redirect to checking_mode functions.
    call_table->begin_func = mon_block_begin_checking_mode;
    call_table->end_func = mon_block_end_checking_mode;
    call_table->stop_func = mon_stop_checking_mode;
    call_table->set_exit_reason_func = set_exit_reason_checking;
}

