#include <stdio.h>
#include <stdlib.h>

#include <papi.h>
#include "perf_monitor.h"
#include "Backends/Papi/papi_mode.h"
#include "Backends/Papi/profiling_papi_mode.h"

counter_t papi_buf[FLUSH_INT];
static FILE* fd = 0;

void flush_counters(void);

/*============= Profiling mode =================*/
void mon_block_begin_profiling_mode(void) {
    PAPI_start_counters((int*)Events, NUM_EVENTS);
}

void mon_block_end_profiling_mode(void) {
    PAPI_stop_counters(papi_buf[buffer_index].values, NUM_EVENTS);
    buffer_index++;

    if (buffer_index == FLUSH_INT) {
       flush_counters();
    }
}

void mon_stop_profiling_mode(void) {
    flush_counters();
    fclose(fd);
    fd = 0;
    printf("PROFILING stops\n");
}

void set_exit_reason_profiling(long exit_reason) {
    papi_buf[buffer_index].exit_reason = exit_reason;
}

void flush_counters(void) {
    int i = 0;

    for (i = 0; i < buffer_index; i++) {
        fprintf(fd, "%ld\t%lld\t%lld\t%lld\t%lld\t%lld\n",
            papi_buf[i].exit_reason,
            papi_buf[i].values[0], papi_buf[i].values[1],
            papi_buf[i].values[2], papi_buf[i].values[3],
            papi_buf[i].values[4]);
    }

    buffer_index = 0;
}

void profiling_init(MonCallTable *call_table, const char *out_fname) {
    printf("PROFILING inits. Writing output to %s\n", out_fname);
    buffer_index = 0;

    if (fd == 0) {
        fd = fopen(out_fname, "wb");
    }

    // Redirect to profiling_mode functions.
    call_table->begin_func = mon_block_begin_profiling_mode;
    call_table->end_func = mon_block_end_profiling_mode;
    call_table->stop_func = mon_stop_profiling_mode;
    call_table->set_exit_reason_func = set_exit_reason_profiling;
}
