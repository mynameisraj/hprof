#include <stdio.h>
#include <stdlib.h>

#include "perf_monitor.h"
#include "Backends/S2E/s2e_mode.h"
#include "Backends/S2E/s2e.h"

static inline void s2e_profile_init(void) {
    __asm__ __volatile__(
        S2E_INSTRUCTION_SIMPLE(60)
    );
}

static inline void s2e_profile_stop(void) {
    __asm__ __volatile__(
        S2E_INSTRUCTION_SIMPLE(61)
    );
}

static inline void s2e_profile_block_begin(void) {
    __asm__ __volatile__(
        S2E_INSTRUCTION_SIMPLE(62)
    );
}

static inline void s2e_profile_block_end(void) {
    __asm__ __volatile__(
        S2E_INSTRUCTION_SIMPLE(63)
    );
}

void mon_block_begin_s2e_mode(void) {
  s2e_profile_block_begin();
  printf("mon_block_begin_s2e_mode\n");
}

void mon_block_end_s2e_mode(void) {
  s2e_profile_block_end();
  printf("mon_block_end_s2e_mode\n");
}

void mon_stop_s2e_mode(void) {
  s2e_profile_stop();
  printf("mon_stop_s2e_mode\n");
}

void profiling_init(MonCallTable *call_table, const char *out_fname) {
  s2e_profile_init();
  printf("S2E profiling inits. Writing output to %s\n", out_fname);

  // Redirect to s2e_mode functions.
  call_table->begin_func = mon_block_begin_s2e_mode;
  call_table->end_func = mon_block_end_s2e_mode;
  call_table->stop_func = mon_stop_s2e_mode;
  call_table->set_exit_reason_func = NULL;
}

void checking_init(MonCallTable *call_table) {
  s2e_profile_init();
  // Redirect to s2e_mode functions.
  call_table->begin_func = mon_block_begin_s2e_mode;
  call_table->end_func = mon_block_end_s2e_mode;
  call_table->stop_func = mon_stop_s2e_mode;
  call_table->set_exit_reason_func = NULL;
}
