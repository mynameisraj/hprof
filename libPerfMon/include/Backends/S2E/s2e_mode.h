#ifndef PERF_MONITOR_S2E_H
#define PERF_MONITOR_S2E_H

#include "perf_monitor.h"

void mon_block_begin_s2e_mode(void);

void mon_block_end_s2e_mode(void);

void mon_stop_s2e_mode(void);

void profiling_init(MonCallTable *call_table, const char *out_fname);

void checking_init(MonCallTable *call_table);

#endif /* PERF_MONITOR_S2E_H */
