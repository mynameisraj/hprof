#ifndef PERF_MONITOR_CHECKING_H
#define PERF_MONITOR_CHECKING_H

#include "perf_monitor.h"

void checking_init(MonCallTable *call_table);

void mon_block_begin_checking_mode(void);

void mon_block_end_checking_mode(void);

void mon_stop_checking_mode(void);

void set_exit_reason_checking(long exit_reason);

#endif /* PERF_MONITOR_CHECKING_H */
