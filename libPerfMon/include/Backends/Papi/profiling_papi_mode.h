#ifndef PERF_MONITOR_PROFILING_H
#define PERF_MONITOR_PROFILING_H

#include "perf_monitor.h"

void profiling_init(MonCallTable *call_table,
                    const char *out_fname);

void mon_block_begin_profiling_mode(void);

void mon_block_end_profiling_mode(void);

void mon_stop_profiling_mode(void);

void set_exit_reason_profiling(long exit_reason);

#endif /* PERF_MONITOR_PROFILING_H */
