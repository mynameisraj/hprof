#ifndef PERF_PAPI_H
#define PERF_PAPI_H

#define NUM_EVENTS 5
static int Events[NUM_EVENTS] = { PAPI_TOT_INS,
                           PAPI_BR_UCN,
                           PAPI_BR_CN,
                           PAPI_SR_INS,
                           PAPI_LD_INS };

#define FLUSH_INT 1000

static long buffer_index = 0;

typedef struct counter_struct {
    long long values[NUM_EVENTS];
    long exit_reason;
} counter_t;

#endif /* PERF_PAPI_H */
