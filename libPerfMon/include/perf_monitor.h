/**
 *  perf_monitor.h
 *
 */

#ifndef PERF_MONITOR_H
#define PERF_MONITOR_H

#define COUNTER_OUTPUT_FILENAME "/tmp/perf_counters.tsv"

typedef  void (*FuncVoidArg)(void);
typedef  void (*FuncLongArg)(long);

typedef struct {
    FuncVoidArg begin_func;
    FuncVoidArg end_func;
    FuncVoidArg stop_func;
    FuncLongArg set_exit_reason_func;
} MonCallTable;

void mon_init(void);

void mon_block_begin(void);

void mon_block_end(void);

void mon_stop(void);

void mon_set_exit_reason(long exit_reason);

#endif /* PERF_MONITOR_H */
