/*
 * hc_wrap.c
 *
 * Created on: Feb 10, 2015
 * Author: Raj Ramamurthy
 */

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include "hypercall.h"

#define KVM_HC_GUEST_MARKING 21 

int main (int argc, char *argv[]) {
  pid_t c_pid, pid;
  int status;
  char* cmd_name;

  if (argc <= 1) {
    fprintf(stderr, "Usage: hcwrap program\n");
    return 0;
  }

  c_pid = fork();
  if (c_pid == 0) {
    // Child. Run given program.
    cmd_name = argv[1];
    execvp(cmd_name, argv+1);
    perror("execve failed");
  } else if (c_pid > 0) {
    // Parent. Wait.
    fprintf(stderr, "hypercall: start\n");
    HYPERCALL1(KVM_HC_GUEST_MARKING, START_MONITORING);
    if ( (pid = wait(&status)) < 0) {
      // Log a stop even in failure
      HYPERCALL1(KVM_HC_GUEST_MARKING, STOP_MONITORING);
      fprintf(stderr, "hypercall: stop\n");
      perror("wait");
      _exit(1);
    }

    // Finished
    HYPERCALL1(KVM_HC_GUEST_MARKING, STOP_MONITORING);
    fprintf(stderr, "hypercall: stop\n");
  } else {
    // Fork failed
    perror("fork failed");
  }

  return 0;
}
