/*
 * hypercall.c
 *
 *  Created on: Feb 1, 2015
 *      Author: Cuong Pham
 */

#include <stdio.h>
#include <unistd.h>
#include "hypercall.h"

#define KVM_HC_GUEST_MARKING 21 

int main (int argc, char *argv[]) {
	int cmd = 0;
	const char *cmd_name = "ZERO";

	if(argc > 1) {
		cmd_name = argv[1];
		if(strcasecmp(argv[1], "START")==0) {
			cmd = START_MONITORING;
		} else if (strcasecmp(argv[1], "STOP")==0) {
			cmd = STOP_MONITORING;
		}
	}	

	HYPERCALL1(KVM_HC_GUEST_MARKING, cmd);
	printf("Made hypercall %s\n", cmd_name);
}
