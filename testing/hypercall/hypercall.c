/*
 * hypercall.c
 *
 *  Created on: Mar 25, 2013
 *      Author: Zak (based on Cuong's)
 */

#include <stdio.h>
#include <unistd.h>
#include "hypercall.h"

int main (int argc, char *argv[]) {
	int cmd = 0;
	const char *cmd_name = "ZERO";

	if(argc > 1) {
		cmd_name = argv[1];
		if(strcasecmp(argv[1], "START")==0) {
			cmd = START_MONITORING;
		} else if (strcasecmp(argv[1], "TICK")==0) {
			cmd = STOP_MONITORING;
		}
	}	

	HYPERCALL0(cmd);
	printf("Made hypercall %s\n", cmd_name);
}
