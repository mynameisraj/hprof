# Graphs the number of unique values over time with a given number of buckets

import matplotlib.pyplot as plt
import sys

if (len(sys.argv) < 3):
    print "usage: data.py filename bucket_size"
    exit(0)

bucket_size = int(sys.argv[2])
buckets_a = set()
buckets_b = set()
count_a = 0
count_b = 0
data_a = []
data_b = []

f = open(sys.argv[1], 'r')
for line in f:
    arr = line.split('\t')
    b = arr[1]
    c = arr[2]
    x = int(b) / bucket_size
    y = int(c) / bucket_size
    if x not in buckets_a:
        count_a += 1
        buckets_a.add(x)
    if y not in buckets_b:
        count_b += 1
        buckets_b.add(y)
    data_a.append(count_a)
    data_b.append(count_b)

data_ax = [i for i in range(0, len(data_a))]
data_bx = [i for i in range(0, len(data_b))]
plt.plot(data_ax, data_a)
plt.plot(data_bx, data_b)
plt.title("Unique Exits Over Time (Bucket Size: " + str(bucket_size) + ")")
plt.xlabel("Number of Exits")
plt.ylabel("Number of Uniqiue Values")
plt.legend(['total', 'branch'], loc='upper left')
plt.show()
