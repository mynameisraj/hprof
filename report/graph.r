run0 <- read.delim("~/Downloads/data/boot0.tsv", header=F)
# See distribution
table(run0$V1)
mmio <- run0[which(run0$V1=="mmio"),]
io <- run0[which(run0$V1=="io"),]

mmio_t <- table(mmio$V2)
png(filename="~/Downloads/mmio_tot_ins.png")
barplot(mmio_t, main="Total Instructions Distribution", xlab="Total Instructions Retired", ylab="Number of Occurrences")
dev.off()

mmio_t <- table(mmio$V3)
png(filename="~/Downloads/mmio_br_ucn.png")
barplot(mmio_t, main="Unconditional Branch Distribution", xlab="Unconditional Branch", ylab="Number of Occurrences")
dev.off()

mmio_t <- table(mmio$V4)
png(filename="~/Downloads/mmio_br_cn.png")
barplot(mmio_t, main="Conditional Branch Distribution", xlab="Conditional Branch", ylab="Number of Occurrences")
dev.off()

mmio_t <- table(mmio$V5)
png(filename="~/Downloads/mmio_sr.png")
barplot(mmio_t, main="Store Distribution", xlab="Store", ylab="Number of Occurrences")
dev.off()

mmio_t <- table(mmio$V6)
png(filename="~/Downloads/mmio_ld.png")
barplot(mmio_t, main="Load Distribution", xlab="Load", ylab="Number of Occurrences")
dev.off()

# io
io_t <- table(io$V2)
png(filename="~/Downloads/io_tot_ins.png")
barplot(mmio_t, main="Total Instructions Distribution", xlab="Total Instructions Retired", ylab="Number of Occurrences")
dev.off()

io_t <- table(io$V3)
png(filename="~/Downloads/io_br_ucn.png")
barplot(io_t, main="Unconditional Branch Distribution", xlab="Unconditional Branch", ylab="Number of Occurrences")
dev.off()

io_t <- table(io$V4)
png(filename="~/Downloads/io_br_cn.png")
barplot(io_t, main="Conditional Branch Distribution", xlab="Conditional Branch", ylab="Number of Occurrences")
dev.off()

io_t <- table(io$V5)
png(filename="~/Downloads/io_sr.png")
barplot(io_t, main="Store Distribution", xlab="Store", ylab="Number of Occurrences")
dev.off()

io_t <- table(io$V6)
png(filename="~/Downloads/io_ld.png")
barplot(io_t, main="Load Distribution", xlab="Load", ylab="Number of Occurrences")
dev.off()

# over time
overtime <- read.delim("~/Downloads/data/over_time/run0_tot.tsv", header=F)
png(filename="~/Downloads/run0_overtime.png")
plot(range(overtime$V1), range(overtime$V2), main="Number of Unique Values Observed Over Time", xlab="Time", ylab="Unique Values Observed So Far",type="n")
lines(overtime$V1, overtime$V2)
dev.off()

