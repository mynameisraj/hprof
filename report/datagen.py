import sys
from sets import Set

def main():
    f = open('data.tsv', 'r')
    vectors = Set()
    count = 0
    for line in f:
        count = count + 1
        arr = line.split('\t')
        for a,b in enumerate(arr):
            if (a == 1):
                if b not in vectors:
                    vectors.add(b)
        print(str(count) + "\t" + str(len(vectors)))


if __name__ == "__main__":
    main()
