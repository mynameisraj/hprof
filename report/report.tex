% vim: textwidth=80
\documentclass[notitlepage]{article}

% packages
\usepackage[pdftex]{graphicx}
\usepackage{listings}
\usepackage{float}
\usepackage{titling}
\usepackage{subfigure}
\usepackage{url}
\usepackage{hyperref}

% set the code to wrap and have box
\lstset{
    frame=single,
    breaklines=true
}

% move the title up
\setlength{\droptitle}{-10em}

\begin{document}

% title
\title{ECE 397 Research Report}
\author{Raj Ramamurthy\\
  \texttt{rmmrthy2 @ illinois.edu}}
\maketitle

% ===== Introduction ==============================
%   1   Background
%   2   Motivation
%   3   Timeline of events
% ===== HPCs ======================================
%   1   Overview
% ===== Literature Review =========================
%   1   Overview
%   2   Using Counters for Dynamic Analysis
%   3   Overhead
%   4   Counter Accuracy and Nondeterminism
%   5   Detection Accuracy from Using HPCs
% ===== PAPI ======================================
%   1   Overview
%   2   Using PAPI to collect data
% ===== QEMU ======================================
%   1   Overview
%   2   Making Modifications to QEMU
% ===== Data Collection ===========================
%   1   Environment
%   2   Data Format and Information
% ===== Analysis ==================================
%	  1   Sources of error in the data
%   2   Graphs (TBD)
%	===== Future work ===============================
%	  1   Building security detection
%	  2   Investigating other code paths
% ==== Conclusion =================================
%   1   Main takeaways from this semester
%   2   Difficulties and lessons learned

\section{Introduction}
This report summarizes my work with the DEPEND research group as part of ECE397
from August 2014 through December 2014.

The paper is structured as follows. A review of related work is provided in
Section II. An overview of hardware performance counters is provided in Section
III. In Section IV, the PAPI library is briefly explained and presented. In
Section V, details relating to QEMU (including how it works with KVM) are
provided. Section VI presents the data and related analysis. Finally, Section
VII summarizes the paper and the semester in addition to providing plans for
future work.

\subsection{Background}
The work this semester initially began as a continuation of the virtual machine
benchmarking and performance isolation work that was started last
semester\cite{vmbench}. However, after I began working on that project, it was
decided that the resources of the group were better spent on different projects
which were more aligned with the group's overall objectives. I met with Cuong
and Zak to discuss project possibilities and chose to work on this one because
it was closely aligned with the same high-level goals of the previous semester's
work.  A week-by-week breakdown of all work completed is shown in
Figure~\ref{fig:worktable}.

This semester was focused on understanding the way we can use hardware
performance counters as a hardware invariant for dynamic
analysis in the hopes of detecting threats in QEMU and KVM. To start, the work
focuses on QEMU.

I sought to answer the following question: \textit{by examining the state of a
    machine at different points in time and comparing these snapshots to the
    previously recorded ones, is it possible to make informed estimations about
    the current threat level or compromisability of a virtualized system?} This
    is a form of \textit{dynamic analysis} because the detection is done at
    runtime and not at compile time (static analysis). The ongoing work to use
    S2E serves as a static counterpart to this work.

\pagebreak

\subsection{Motivation}
To understand the motivation behind this project, it is important to have a good
high-level understanding of the virtualized system topology. A visual overview
of the topology is shown in Figure~\ref{fig:vmtop}.

\begin{figure}[htp]
    \centering
    \includegraphics[width=3.5in]{container_diagram.pdf}
    \caption{Virtual machine topology. Areas of focus include QEMU and KVM.}
    \label{fig:vmtop}
\end{figure}

A search of the National Vulnerability Database shows that there are many
reported vulnerabilities in QEMU. These range in severity, but can be as grave
as allowing for arbitrary code execution by the virtual machine
guest.\footnote{\url{http://web.nvd.nist.gov/view/vuln/search-results?query=QEMU&search_type=all&cves=on}}
While there are research papers already published on dynamic analysis using
hardware performance counters, these have not been applied towards
virtualization\cite{numchecker}\cite{feasibility}\cite{pc}. While the combined
focus of this project is unique, it is relatable and builds upon existing work.
A summary of this existing work follows in Section III.

% TODO: figure out placement of this figure
\begin{figure}[ht!]
  \begin{tabular}{| p{1cm} | p{10cm} |}
    \hline
    \textbf{Week} & \textbf{Status}\\
    \hline
    1 & Initial meeting of the semester. Talk about abstract which was
    published, how to begin implementation. Understanding changes in container
    landscape. \\
    \hline
    2 & Research of APIs to programmatically create and manage virtual machines.
    Lay out plan for implementation. Reading about prior work in automation. \\
    \hline
    3 & Getting set up with the API on the machine. Troubleshooting. \\
    \hline
    4 & Initial ``hello world'' example working for the benchmark. Very simple
    at this point. \\
    \hline
    5-7 & In limbo about the project due to complications with the group. Somewhat
    stalled. \\
    \hline
    8 & Begin talking about the possible projects. S2E, Python project, and
    this. \\
    \hline
    9 & Understanding the counters of interest. Work to add counters to QEMU
    and get a basic PAPI setup working. \\
    \hline
    10 & Difficulty with machines due to PAPI not being able to work on assigned
    machine. Lots of various setup  and installation problems encountered and
    fixed. \\
    \hline
    11 & Get the PAPI and QEMU implementation working, but it is very slow.
    Investigating slowness and how PAPI is susceptible to change. \\
    \hline
    12 & More theoretical discussion. Can we send signals to observe changes in
    the performance counter variables? How is PAPI virtualizing the counters? \\
    \hline
    13-14 & Inactive due to midterms and Thanksgiving break. Unfortunately
    realize that the values being collected are for the wrong codepath. \\
    \hline
    15 & Over break, worked on slowness problems. Re-implement data collector
    and run sample. Much faster this time. \\
    \hline
    16 & With properly collected data, begin discussing analysis. \\
    \hline
  \end{tabular}
  \caption{A high level overview of the work accomplished each week.}
  \label{fig:worktable}
\end{figure}

\pagebreak
% HPCs
% 1) Overview
\section{Hardware Performance Counters}
Hardware performance counters (herein referred to as HPCs) allow for the
collection of various low-level information about a processor's current state.
The counters can provide valuable performance insights. Unfortunately, the first
machine I attempted to use did not support HPCs, and it is not enabled by
default on most machines.  However, after switching to another machine, I was
able to enable the counters and begin collecting data.

The counters available vary from machine to machine. The important counters are
total retired instructions, conditional branch instructions, branch
instructions, store instructions, and load instructions.  The counters were
collected with PAPI, which is described in the next section.

% Literature Review
% 1) Overview
% 2) Using Counters for Dynamic Analysis
% 3) Overhead
% 4) Counter Accuracy and Nondeterminism
% 5) Detection Accuracy from Using HPCs
\section{Literature Review}
There are a number of existing works which cover HPCs for security. These center
around several concerns:
\begin{itemize}
    \item Overhead in using HPCs
    \item Accuracy and nondeterminism of the counters
    \item Accuracy of detection methods using counters
    \item Feasability of HPCs as a dynamic analysis invariant
\end{itemize}

\subsection{Using Counters for Dynamic Analysis}
The paper ``Are Hardware Performance Counters a Cost Effective Way for Integrity
Checking of Programs?'' attempts to understand performance counter usage for
integrity checking from a cost perspective, and concludes that HPCs are
efficient for detecting program modification\cite{arehardware}. The
NumChecker\cite{numchecker} paper presents a framework through which to detect
kernel rootkits. It uses HPCs to verify system call execution.

\subsection{Overhead}
As the name implies, hardware performance counters were initially designed to
aid in performance instrumentation of low-level software. Using HPCs is
attractive because they are built into the processor, meaning that they add a
negligible amount of hardware overhead to program execution. However, storing
this information and analyzing it dynamically can introduce overhead. The
NumChecker\cite{numchecker} paper reveals that the amount of overhead varies
significantly with the frequency of collection,  presumably due to I/O; for a
sampling rate of 5 seconds, the overhead introduced by NumChecker was
2.8\%\cite{numchecker}. Another work shows that the overhead incurred in that
situation was less than 10\%\cite{arehardware}.

\subsection{Counter Accuracy and Nondeterminism}
Using HPCs as an invariant has proved a controversial topic, as they appear to
be nondeterministic. This is because there are many events happening on the
processor, which cause small fluctuations in the counter values.

However, some metrics are more consistent than others. From \cite{arehardware}:
``The shortlisted events include the total number of instructions retired, the
number of branch instructions retired, cache stores (they are better measures
than loads because loads can be speculative), completed I/O operations, and
number of floating point operations.''\cite{arehardware}

There are many sources of nondeterminism, and it appears to vary from chip to
chip. The paper ``Non-determinism and overcount on modern hardware performance
counter implementations'' explores these sources and attempts to make sense of
which counters are best. It comes to the conclusion that total instructions
retired is one of the most consistent counters\cite{overcount}.

\subsection{Detection Accuracy from Using HPCs}
HPCs only provide the data. From this data, is important to conduct proper
statistical analysis to achieve meaningful results. Specifically, in integrity
checking, the most important metric is a low false positive rate.

The HPC data is best interpreted as a matrix of feature vectors.  From this, a
number of very interesting analytical techniques arise, including (but not
limited to): linear regression, support vector machines, random forests, nearest
neighbors, and clustering. Principally, nearest neighbors is the most popular of
these choices, but each approach has different advantages\cite{forsyth}. It is
also possible to build simple classification simply by looking within a
threshold of all known values\cite{numchecker}. In one paper, artificial neural
networks were used\cite{feasibility}.

% PAPI
% 1) Overview
% 2) Using PAPI to collect data
\section{PAPI}
As described by the maintainers, ``The Performance API (PAPI) project specifies
a standard application programming interface (API) for accessing hardware
performance counters available on most modern microprocessors. These counters
exist as a small set of registers that count Events, occurrences of specific
signals related to the processor's function.''\cite{papi}

PAPI was an ideal choice for this project because it offers a portable library,
very thorough documentation, and allows for the usage of HPC APIs as provided by
the chipmaker from a much higher level than assembly. PAPI abstracts away
architecture-specific counters and makes gathering HPC data much more portable.
It is actively maintained and has a large amount of activity and support from a
wide variety of industry leaders.

\subsection{Using PAPI to collect data}
The PAPI API is fairly simple. Simply running the tool \texttt{papi-avail} will
show the list of available counters on a given machine. From this list, the
counters as described in Section III were gathered. A small code sample is
below:

% TODO: get the right PAPI instruction #define for total floating point
% operations
\begin{figure}[htp]
\begin{verbatim}
#include <papi.h>
#define NUM_EVENTS 2
int Events[NUM_EVENTS] = { PAPI_TOT_INS, PAPI_FDV_INS };
long long values[NUM_EVENTS];

int main() {
    int i, j;
    PAPI_start_counters((int*)Events, NUM_EVENTS);
    // Do some work here
    for (i = 0; i < 100; i++) {
       j = i*10 % 3;  // Perform a floating point operation
    }
    PAPI_stop_counters(values, NUM_EVENTS);
    // Counters are now placed in the array `values`
    return 0;
}
\end{verbatim}
\caption{A small sample program to collect total instructions counted.}
\end{figure}

% QEMU
% 1) Overview
% 2) Making Modifications to QEMU
\newpage
\section{QEMU}
QEMU is a machine emulator which allows for the emulation of a platform so that
a full operating system/hypervisor may run on top of it. QEMU provides emulation
of both entire machines and external I/O devices (e.g., hard-drives, network
devices, and keyboards). However, when the host and guest operating system are
the same architecture\footnote{An \texttt{x86} machine may also be virtualized
on an \texttt{x86\_64} architecture}, native virtualization can be accomplished
by using KVM, a Linux kernel feature which allows for hardware-assisted
virtualization. KVM leverages processor support for virtualization to expose an
API for running guest virtual machines. In this configuration, QEMU only
emulates I/O devices.  Because both pieces of software play crucial roles in
running the guest virtual machine, execution frequently traps from one to
another. This is explained in Figure~\ref{fig:qemutrap}.

\begin{figure}
    \centering    
    \includegraphics[width=4in]{qemu_trap.pdf}
    \caption{Diagram of the various transfers of control between QEMU, KVM, and
        the host virtual machine. When KVM returns to QEMU, it provides an exit
        reason which can be used by QEMU to take appropriate action. However,
        KVM may transfer control to the virtual machine (and back) before
        returning to QEMU. The arrows represent transfer of control. The
        duration for which HPCs were recorded is also displayed. The area of
        interest highlighted was chosen because it focuses solely on QEMU while
        allowing for collection of the exit reason from KVM.}
\label{fig:qemutrap}
\end{figure}

\subsection{Making Modifications to QEMU}
Unfortunately, ``QEMU does not have a high level design description document -
only the source code tells the full story.'' Fortunately, since this project is
primarily concerned with KVM and QEMU inter-opteration, only a single source
code file (\texttt{kvm-all.c}) required modification.  However, because the
project requires linking QEMU with PAPI, some modifications must be made to
QEMU's generated Makefiles on a per-architecture basis (QEMU builds each target
architecture in its own folder). A patch file was generated so that this
development is easier moving forward.

% Data Collection
% 1) Environment
% 2) Data Format and Information
\section{Data Collection}
\subsection{Environment}
The data was collected on the \texttt{strauss} machine, which runs Ubuntu
14.04.1 LTS and has a 64 bit Intel i3 chip clocked at 3.0 GHz. THe machine has 8
GB of memory. For this research, the latest QEMU (2.1.2) was obtained from
upstream as several modifications had been made to improve KVM support. PAPI
version 5.3 was downloaded and installed for this project.

\subsection{Data Format and Information}
To collect the data, a special modified version of QEMU's \texttt{system-x86\_64}
emulator was built and run. The data was collected for every KVM exit
that trapped to QEMU. The PAPI performance counters were set to zero
\textit{after} every exit, and the data was collected immediately
\textit{before} the next entry. This was to ensure that the codepath measured was
only that of QEMU and not including KVM. The following data were recorded:
\begin{itemize}
    \item{KVM exit reason. While collection was turned on for every KVM exit
            reason, the emulator only saw exits from KVM with the reasons
            \texttt{MMIO} (memory mapped I/O) and \texttt{IO}. This was
            unexpected because KVM has many potential exit reasons. Future work
            will investigate this and try to trigger the other exit reasons.}
    \item{Total instructions retired}
    \item{Total unconditional branch instructions retired}
    \item{Total conditional branch instructions retired}
    \item{Total store instructions retired}
    \item{Total load instructions retired}
\end{itemize}
The above comprises one data entry. Each entry was written to an in-memory buffer
of $10000$ entries which in turn flushed to disk in a tab-separated file once
full. The data run, or workload, encompassed the boot of the virtual machine
until the login screen was first available. A total of $450000$ data points were
collected for a single boot.

% Analysis
%	1) Understanding discrete ranges in the generated data
%	2) Sources of error in the data
%	3) Future work
%	   A) Building security detection functionality based on the data
%	   B) Investigating other areas of QEMU/KVM with performance counters
\section{Analysis}

% PAPI_TOT_INS, PAPI_BR_UCN, PAPI_BR_CN, PAPI_SR_INS, PAPI_LD_INS
\subsection{Data Overview}
Of the data points, $93\%$ represented IO exits, and the other $7\%$ represented 
MMIO exits. \\

\begin{center}
\begin{tabular}{| l | l | l |}
    \hline
    \textbf{Exit Reason} & \textbf{HPC observed} & \textbf{Unique Values} \\
    \hline
    IO & Total Instructions & 1116 \\
    \hline
    IO & Unconditional branch & 1348 \\
    \hline
    IO & Conditional branch & 1594 \\
    \hline
    IO & Store & 845 \\
    \hline
    IO & Load & 935 \\
    \hline
    MMIO & Total Instructions & 176 \\
    \hline
    MMIO & Unconditional branch & 287 \\
    \hline
    MMIO & Conditional branch & 286 \\
    \hline
    MMIO & Store & 122 \\
    \hline
    MMIO & Load & 147 \\
    \hline
\end{tabular}
\end{center}

At a high level, this data reveals that observing the same value for a HPC
multiple times is actually fairly common. However, in both of these datasets,
there are multiple values which are observed only once.\footnote{To say a value
    is observed only once means that a certain value for the HPC in the area of
interest is not repeated.} These are likely due to nondeterminism, but not from
the HPC. Because we cannot know the QEMU codepath to be taken ahead of time (one
of the drawbacks of dynamic analysis), there is some nondeterminism introduced
by QEMU.

\begin{figure}[htp]
    \centering
    \includegraphics[width=3.5in]{tot_ins_bar_mmio.pdf}
    \caption{Distribution of unique values observed for total instructions
    retired for MMIO exits. Visually, it is clear that the values tend to
cluster together. Furthermore, observing the scale shows that values are usually
in the thousands and not in the hundreds of thousands. No outliers were removed
for this bar plot, nor were any values found only once. The x-axis scale is
intended to show a bucket for each value observed.}
\end{figure}

\begin{figure}[htp]
    \centering
    \includegraphics[width=3.5in]{tot_ins_bar_io.pdf}
    \caption{Distribution of unique values observed for total instructions
        retired for IO exits. The IO exits have a much larger range, but they
        are also a much larger dataset. Almost all exits from KVM happen under
        the reason \texttt{IO}. In both of these graphs, there are a number of spikes
        in the bar chart which indicate that a single codepath is being hit
    multiple times. Presumably, nearby spikes correspond to similar codepaths.}
\end{figure}
\pagebreak
\subsection{Correlation Between Different Counters}
When observing multiple features, it is often useful to compute the correlation
coefficient between pairs of them. In the case of the collected data, the number
of instructions retired is a ``primary'' feature in that it should have strong
positive correlation between all of the other features. In computing the
correlation coefficients, it was observed that they are all above $0.9$, which
indicates strong correlation. This makes sense. If there are more branches,
stores, or loads, then there should be more total instructions retired. The
values of correlation are shown in Figure~\ref{fig:cortable}.

\begin{figure}[htp]
  \centering
  \begin{tabular}{| p{5cm} | p{5cm} |}
      \hline
      \textbf{Counter} & \textbf{Correlation to Total (R)} \\
      \hline
      Unconditional Branch & $0.9948598$ \\
      \hline
      Conditional Branch & $0.9803158$ \\
      \hline
      Store & $0.9938992$ \\
      \hline
      Load & $0.965551$ \\
      \hline
  \end{tabular}
  \caption{Correlation values for total instructions retired and the various
      other counters, as labeled. These values are for all exit reasons. The
  high value of R indicates these counters are strongly correlated.}
  \label{fig:cortable}
\end{figure}

\subsection{A Time-Based Analysis of the Data}
In order to understand how the values in the table are propagated over time, I
created a plot of the number of unique values over time. Since each successive
row of the data from QEMU is collected after its preceding rows, a new dataset
can be constructed. In order to do this I wrote a small Python program to
populate a set as each value was read in. This data was then plotted on a
scatter plot, the results of which are shown in Figure~\ref{fig:overtime}.

\begin{figure}[htp]
    \centering
    \includegraphics[width=4.5in]{values_over_time.pdf}
    \caption{Number of unique values observed over time. This is for all exit
        types and for the total instructions retired HPC. The graph shows that
        at a certain point in time (roughly $2\times10^5$ units), many more
        unique events begin occurring. This can be linked to the Linux boot
    sequence.  Once many different programs start being executed by the kernel
(after init), the number of unique I/O operations increases. Future work will
examine this in greater detail.}
    \label{fig:overtime}
\end{figure}

\newpage
% Conclusion
% 1) Main takeaways from this semester
% 2) Difficulties and lessons learned from setup
\section{Conclusion}
This semester, I learned about how QEMU and KVM interact with one another and
about using hardware performance counters as an invariant for dynamic
invalidation detection. I enjoyed this project and look forward to seeing it
develop into something which can be published. I had no security background
going in to this project, so it was interesting to see how security work takes
place and read papers about doing dynamic analysis.

\subsection{Future Work}
The data presents some very interesting directions to take this work.
\begin{itemize}
    \item Use a code coverage tool such as \texttt{gcov} to examine which
        codepaths are taken. This would be very helpful in understanding why
        certain spikes occur and what codepath they are taking. It would provide
        evidence to support (or refute) the hypothesis that spikes in the
        barplots are from the same codepath.
    \item Generate multiple sets of data for training, test, and validation
        purposes. The existing data can all be classified as ``normal'', so this
        would include adding test vulnerabilities with which to classify. Then
        build a classifier for this data using some of the strategies outlined
        in Section III.
    \item Look further into the point in time where the unique data points begin
        accumulating at a much greater rate. This could involve keeping track of
        the number of unique data points in code and engaging a breakpoint when
        the rate of increase of the size of the set reaches a certain threshold.
    \item Write code to do detection in real time. Experiment with the sampling
        rate to develop a solution which has minimal overhead but relatively low
        time-to-detection.
    \item Perform this same analysis on different scenarios and automate it.
        This includes different operating systems and different workloads.
    \item For MMIO-related KVM exits, figure out what source the I/O came from
        (network, disk, etc). Also look at generating numbers for what a
        ``normal'' exit should be.
\end{itemize}

% list all references without citing them in the document
\nocite{*}

\bibliography{report}{}
\bibliographystyle{plain}

\end{document}

