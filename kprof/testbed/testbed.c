/* 
 * Testbed for the kprof kernel module.
 * Written by Raj Ramamurthy for DEPEND Research Group in April 2015.
 */

#include <linux/module.h>
#include <linux/kernel.h>

#define TEST_EXIT_REASON 500
#define NLOOPS 100

/* Symbols from the kprof module */
extern void start_record(unsigned long exit_reason);
extern void stop_record(void);
extern void kp_log_mark(void);

static void run_test(void)
{
    int i, next, first = 0, second = 1;
    
    printk(KERN_INFO "kp_testbed: running test\n");
    start_record(TEST_EXIT_REASON);
    /* Do some sample work, such as computing fibonacci */
    for (i = 0; i < NLOOPS; i++) {
        if (i <= 1) {
            next = i;
        } else {
            next = first + second;
            first = second;
            second = next;
        }
    }
    stop_record();
    printk(KERN_INFO "kp_testbed: test complete\n");
}

static int __init kp_testbed_init(void)
{
    printk(KERN_INFO "kp_testbed: init\n");
    run_test();
    return 0;
}

static void __exit kp_testbed_exit(void)
{
    printk(KERN_INFO "kp_testbed: exit\n");
}

module_init(kp_testbed_init);
module_exit(kp_testbed_exit);
MODULE_LICENSE("GPL");
