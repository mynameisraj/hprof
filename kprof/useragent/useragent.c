/* 
 * User-land buf-logging component for the kprof module.
 * Written by Raj Ramamurthy for DEPEND Research Group in April 2015.
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/ioctl.h> /* ioctl functions */
#include <fcntl.h>     /* open */
#include <string.h>    /* strerror */
#include <errno.h>
#include "../include/kprof_common.h"

#define DEFAULT_OUTFILE "out.tsv"
#define NUM_BUFS 100

log_buffer_t buf[NUM_BUFS];
FILE *outfile;
void handler(int);

/*
 * Print out all counters that do not have garbage values in the buffer.
 */
void print_log_buf(log_buffer_t buf[], FILE* outfile)
{
    unsigned int i;
    unsigned int j;
    unsigned long total_events = 0;
    for (i = 0; i < NUM_BUFS; i++) {
        total_events += buf[i].count;
        for (j = 0; j < buf[i].count; j++) {
            fprintf(outfile, "%lu\t%lld\t%lld\n",  buf[i].events[j].exit_reason,
                    buf[i].events[j].tot_ins, buf[i].events[j].branch);
        }
    }
}

int main(int argc, char **argv)
{
    int file_desc,  ret, i, cur_buf;
    char *outfile_name;

    // Signal handler
    signal(SIGINT, handler);

    // Parse out the argument
    if (argc < 2) {
        printf("No explicit outfile provided. Using %s\n", DEFAULT_OUTFILE);
        outfile_name = DEFAULT_OUTFILE;
    } else {
        outfile_name = argv[1];
    }
    outfile = fopen(outfile_name, "w");

    file_desc = open("/dev/kprof", O_RDWR);
    if (file_desc < 0) { 
        printf("failed to open /dev/kprof. Did you run as root?\n");
        return -1;
    }

    for (i = 0; i < NUM_BUFS; i++) {
        buf[i].count = 0;
        buf[i].events = malloc(sizeof(kp_perf_val_t)*BUF_LIMIT);
    }
    cur_buf = 0;

    // Reset the module
    ioctl(file_desc, KP_RESET_LOG);

    while (cur_buf < NUM_BUFS) {
        /* 
         * We may need to make several calls because the module throttles us.
         */
        while (buf[cur_buf].count == 0) {
            ret = ioctl(file_desc, KP_READ_LOG, &buf[cur_buf]);
        }
        printf("received %ld events\n", buf[cur_buf].count);
        cur_buf++;
    }

    close(file_desc);
    if (ret >= 0) {
        print_log_buf(buf, outfile); 
    } else {
        printf("error: %s\n", strerror(errno));
    }

    for (i = 0; i < NUM_BUFS; i++) {
        free(buf[i].events);
    }
    close(outfile);
    return 0;
}

void handler(int sig)
{
    print_log_buf(buf, outfile); 
    exit(0);
}
