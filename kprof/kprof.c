/* 
 * Kernel module which gathers performance counter data.
 * Written by Raj Ramamurthy for DEPEND Research Group in April 2015.
 *
 * Performance counters adapted from:
 * http://lxr.free-electrons.com/source/kernel/watchdog.c#L490
 *
 * See this link for documentation:
 * http://web.eece.maine.edu/~vweaver/projects/perf_events/perf_event_open.html
 *
 * Lock-free buffer copying provided by Cuong Pham.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/perf_event.h>
#include <linux/ioctl.h>   /* ioctl control */
#include <linux/fs.h>      /* file_operations */
#include <linux/types.h>   /* dev_t */
#include <linux/kdev_t.h>  /* dev_t macros */
#include <linux/cdev.h>    /* cdev */
#include <linux/slab.h>    /* kmalloc */
#include <asm/uaccess.h>   /* get_user and put_user */
#include <linux/device.h>  /* device creation */

#include "include/kprof_ioctl.h"

/***** perf counters *****/
static long counters[NUM_COUNTERS] = {PERF_COUNT_HW_INSTRUCTIONS, PERF_COUNT_HW_BRANCH_INSTRUCTIONS};
static long counter_types[NUM_COUNTERS] = {PERF_TYPE_HARDWARE, PERF_TYPE_HARDWARE};
static DEFINE_SPINLOCK(record_lock);
static struct perf_event *events[NUM_COUNTERS];
static long long perf_counts[NUM_COUNTERS];
static int perf_event_enabled = 0;
unsigned long last_exit_reason;

/***** buffer *****/
static log_buffer_t log_buf[NUM_BUCKETS];
static int current_log_buffer;
static int out_buffer = 0; /* currently outputting this one */

/***** ioctl *****/
static dev_t first;
static int major_number = 0;
static struct cdev *kp_dev;
static struct class *cl;
static long kp_ioctl(struct file *filp, unsigned int cmd, unsigned long arg);
static struct file_operations g_fops = {
    .unlocked_ioctl = kp_ioctl,
    .owner = THIS_MODULE
};


static void reset_log_buffer(void)
{
    int i;
    for (i = 0; i < NUM_BUCKETS; ++i) {
        log_buf[i].count = 0;
        perf_event_disable(events[i]);
    }
    perf_event_enabled = 0;
    current_log_buffer = 0;
    out_buffer = 1;
}

static long kp_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
    int tmp;
    int ret = EXIT_SUCCESS;
    log_buffer_t *buf;

    if (!perf_event_enabled) {
        return EXIT_FAILURE;
    }

    switch (cmd)
    {
        case KP_RESET_LOG:
            printk(KERN_INFO "KP_RESET_LOG\n");
            reset_log_buffer();
            break;
        case KP_READ_LOG:
            if (log_buf[current_log_buffer].count < IOCTL_THRESHOLD) {
                tmp = 0;
                ret = copy_to_user(&(buf->count), &tmp, sizeof(buf->count));
            } else {
                buf = (log_buffer_t *) arg;
                printk(KERN_INFO "KP_READ_LOG\n");

                // Figure out which buffer to use
                out_buffer = current_log_buffer;
                current_log_buffer = (current_log_buffer + 1) % NUM_BUCKETS;

                // Copy the count and events separately
                ret = copy_to_user(&(buf->count),
                                   &(log_buf[out_buffer].count),
                                   sizeof(buf->count));
                if (ret != 0) {
                    printk(KERN_ERR "copy_to_user: error (1): %d\n", ret);
                    return -EFAULT;
                }

                /*
                 * We need to be careful here, because this assumes a large
                 * enough region is allocated in user-space
                 */
                ret = copy_to_user(buf->events, log_buf[out_buffer].events,
                                   sizeof(kp_perf_val_t)*BUF_LIMIT);
                if (ret != 0) {
                    printk(KERN_ERR "copy_to_user: error (2): %d\n", ret);
                    return -EFAULT;
                }

                // Reset the slot on the out buffer
                log_buf[out_buffer].count = 0;
            }

            break;
    }
    return EXIT_SUCCESS;
}

/*
 * Begin reading the latest value of the performance counters. They are already
 * initialized by the module initializer.
 */
void start_record(unsigned long exit_reason)
{
    long long enabled, running;
    int i;
    if (!perf_event_enabled) perf_event_enabled = 1;
    last_exit_reason = exit_reason;
    spin_lock(&record_lock);
    for (i = 0; i < NUM_COUNTERS; ++i) {
        perf_event_enable(events[i]);
        perf_counts[i] = perf_event_read_value(events[i], &enabled, &running);
    }
    spin_unlock(&record_lock);
}
EXPORT_SYMBOL(start_record);

/*
 * Obtain the difference in the counter value since the last time
 * start_record() was invoked
 */
void stop_record(void)
{
    long long enabled, running;
    int idx, i;
    long long vals[NUM_COUNTERS];

    // In instances where stop is called first, avoid logging anything
    if (!perf_event_enabled) return;

    spin_lock(&record_lock);
    for (i = 0; i < NUM_COUNTERS; ++i) {
        perf_event_disable(events[i]);
        vals[i] = perf_event_read_value(events[i], &enabled, &running)-perf_counts[i];
    }
    spin_unlock(&record_lock);

    // Log it into the buffer
    idx = log_buf[current_log_buffer].count;
    if (idx < BUF_LIMIT) {
        log_buf[current_log_buffer].events[idx].tot_ins = vals[0];
        log_buf[current_log_buffer].events[idx].branch = vals[1];
        log_buf[current_log_buffer].events[idx].exit_reason = last_exit_reason;
        ++(log_buf[current_log_buffer].count);
    }
}
EXPORT_SYMBOL(stop_record);

/* Insert a new "dummy" row into the data to indicate a marker */
void kp_log_mark(void)
{
    int idx = log_buf[current_log_buffer].count;
    if (idx < BUF_LIMIT) {
        log_buf[current_log_buffer].events[idx].tot_ins = 0;
        log_buf[current_log_buffer].events[idx].branch = 0;
        log_buf[current_log_buffer].events[idx].exit_reason = KP_MARK_EXIT;
        ++(log_buf[current_log_buffer].count);
    }
}
EXPORT_SYMBOL(kp_log_mark);
    
static int __init kp_module_init(void)
{
    int ret, i;
    struct perf_event_attr pe;
    struct device *dev_ret;

    // Set up the performance counters
    memset(&pe, 0, sizeof(struct perf_event_attr));
    for (i = 0; i < NUM_COUNTERS; ++i) {
        pe.type = counter_types[i];
        pe.size = sizeof(struct perf_event_attr);
        pe.config = counters[i];
        pe.disabled = 1;
        pe.exclude_user = 1; // Exclude events that happen in user space
        events[i] = perf_event_create_kernel_counter(&pe, 0, NULL, NULL, NULL);
    }

    // Set up the device and make it accessible from /dev
    ret = alloc_chrdev_region(&first, 0, 1, DEVICE_NAME);
    if (ret < 0) {
          printk(KERN_ERR "failed to allocate major number\n");
          return ret;
    }
    if (IS_ERR(cl = class_create(THIS_MODULE, CLASS_NAME))) {
        unregister_chrdev_region(first, 1);
        return PTR_ERR(cl);
    }
    if (IS_ERR(dev_ret = device_create(cl, NULL, first, NULL, DEV_NAME))) {
        class_destroy(cl);
        unregister_chrdev_region(first, 1);
        return PTR_ERR(dev_ret);
    }
    major_number = MAJOR(first);
    printk(KERN_INFO "major number is %d\n", major_number);
    kp_dev = cdev_alloc();
    kp_dev->ops = &g_fops;
    kp_dev->owner = THIS_MODULE;

    ret = cdev_add(kp_dev, first, 1);
    if (ret < 0) {
        printk(KERN_ERR "cdev_add failed: %d\n", ret);
        device_destroy(cl, first);
        class_destroy(cl);
        unregister_chrdev_region(first, 1);
        return ret;
    }

    // Allocate memory for the buckets
    for (i = 0; i < NUM_BUCKETS; i++) {
        log_buf[i].events = kmalloc(sizeof(kp_perf_val_t)*BUF_LIMIT, GFP_KERNEL);
        log_buf[i].count = 0;
        if (!log_buf[i].events) {
            printk(KERN_ERR "kmalloc failed on bucket %d\n", i);
            return EXIT_FAILURE;
        }
    }

    printk(KERN_INFO "kp_module_init()\n");
    return EXIT_SUCCESS;
}

static void __exit kp_module_exit(void)
{
    int i;
    // Free bucket memory
    for (i = 0; i < NUM_BUCKETS; i++) {
        kfree(log_buf[i].events);
    }

    // Remove the /dev entry and character device
    cdev_del(kp_dev);
    device_destroy(cl, first);
    class_destroy(cl);
    unregister_chrdev_region(first, 1);
    
    printk(KERN_INFO "kp_module_exit()\n");
}

module_init(kp_module_init);
module_exit(kp_module_exit);
MODULE_DESCRIPTION("DEPEND performance counter driver");
MODULE_AUTHOR("Raj Ramamurthy");
MODULE_LICENSE("GPL");
