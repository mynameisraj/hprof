/* 
 * Common defines for the kprof kernel module and userspace helper.
 * Written by Raj Ramamurthy for DEPEND Research Group in April 2015.
 *
 * This file is shared between user and kernel space (hence "common").
 */

#ifndef HPROF_COMMON_H
#define HPROF_COMMON_H

#include <linux/ioctl.h>   /* ioctl control */

#define KP_IOCTL_NO 44
#define KP_READ_LOG _IOR(KP_IOCTL_NO, 2, log_buffer_t)
#define KP_RESET_LOG _IO(KP_IOCTL_NO, 3)

/*
 * To avoid the use of locks, alternating buffers are used. Furthermore, to
 * increase performance, adjust the threshold higher.
 */
#define NUM_BUCKETS 2
#define BUF_LIMIT 100000
#define IOCTL_THRESHOLD BUF_LIMIT / 4
#define NUM_COUNTERS 2
#define KP_MARK_EXIT 1 << 2

typedef struct kp_perf_val {
    long long tot_ins;
    long long branch;
    unsigned long exit_reason;
} kp_perf_val_t;

/*
 * WARNING: events is an array that should always have space for BUF_LIMIT
 * elements. The reason this is not done statically is because it will cause a
 * compilation error from the induced use of `sizeof`. The code will not read
 * garbage values and will use the `count` field to know how far to read.
 */
typedef struct log_buffer {
    long count;
    kp_perf_val_t *events;
} log_buffer_t;

#endif
