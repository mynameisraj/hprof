/* 
 * Contains kernel module specific definitions for kprof.
 * Written by Raj Ramamurthy for DEPEND Research Group in April 2015.
 */

#ifndef KPROF_H
#define KPROF_H

#include <linux/ioctl.h>    /* for device I/O control */
#include "kprof_common.h"

#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1

// The formal device name as shown in /proc/devices
#define DEVICE_NAME "kprof"
#define CLASS_NAME "depend"
// Under /dev, the module is accessible from this
#define DEV_NAME DEVICE_NAME

/***** kernel module exports *****/
void start_record(unsigned long exit_reason);
void stop_record(void);
void kp_log_mark(void);

#endif
