#include <stdlib.h>
#include <stdio.h>

#include <papi.h>
#define NUM_EVENTS 5
int Events[NUM_EVENTS] = { PAPI_TOT_INS,
                           PAPI_BR_UCN,
                           PAPI_BR_CN,
                           PAPI_SR_INS,
                           PAPI_LD_INS };

#define DEPEND_FILENAME "/tmp/small_loop_counters.tsv"
#define FLUSH_INT 1000
static int counter = 0;
typedef struct depend_papi_struct {
    int long values[NUM_EVENTS];
} dp_t;
dp_t papi_buf[FLUSH_INT];

#define NLOOPS 16 
#define SMALL_LOOP 10

void flush_counters();
void fool(const int idx1, const int var1,
         const int idx2, const int var2);
void no_access_zone();

void main() {
  int i = 0;

  for (i = 6; i < NLOOPS; ++i) {
    fool(i, i, i, i);
  }

}

static FILE* fd = 0;
void flush_counters() {
    int i = 0;

    if (fd == 0) {
        fd = fopen(DEPEND_FILENAME, "wb");
    }

    for (i = 0; i < counter; i++) {
        fprintf(fd, "%lld\t%lld\t%lld\t%lld\t%lld\n",
            papi_buf[i].values[0], papi_buf[i].values[1],
            papi_buf[i].values[2], papi_buf[i].values[3],
            papi_buf[i].values[4]);
    }

    counter = 0;
}

void fool(const int idx1, const int var1,
         const int idx2, const int var2)
{
   int array[6];
//   int i = 0;

//   printf("ret addr = %p\n", __builtin_return_address(0));
//   printf("&array[0] = %p\n", &array[0]);
//   printf("_ - _ = %x\n", (unsigned int)array - (unsigned int)__builtin_return_address(0));
//   printf("&array[idx1] = %p\n", &array[idx1]);
   // printf("idx1 = 0x%lx\n", idx1);
   printf("var1 = %d\n", var1);

   array[idx1] = (int) no_access_zone;
   // for (i = 0; i > idx1; --i) {
   //  printf("i=%ld; %p\n", i, &array[i]);
   //}

   // malicious user might set idx1 to the offset of the return address,
   // and var1 to an address to execute.
//   array[idx1] = var1;
//   array[idx2] = var2;

//   printf("%ld\t%ld\t%ld\t%ld\t%ld\n",
//     array[0], array[1], array[2], array[3], array[4]);
}

void no_access_zone() {
   printf("no_access_zone\n");
   while (1) {}
}