#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "perf_monitor.h"

#define OUTPUT_FILENAME "/tmp/rand.tsv"

#define NLOOPS 30000
#define SMALL_LOOP 10
#define ARRAY_SIZE 4
static FILE* outputFile = 0;

void fool(const long idx1, const long var1);
void no_access_zone();
void free_jumper();

int main() {
  long i = 0;
  srand(time(NULL));
  mon_init();

  outputFile = fopen(OUTPUT_FILENAME, "wb");

  for (i = 0; i < NLOOPS; ++i) {
    mon_block_begin();

    if (i % 5000 == 5001) {
      // Code injection
      fool(ARRAY_SIZE + 3, (long) (free_jumper) + 4);
    } else {
      fool(i % ARRAY_SIZE, (long) 100);
    }

    mon_block_end();
  }

  fclose(outputFile);

  mon_stop();

  return 0;
}

void fool(const long idx1, const long var1) {
   long array[ARRAY_SIZE];
   long i = 0;

   for (i = 0; i < ARRAY_SIZE; ++i) {
     array[i] = rand();
   }
   array[idx1] = var1;

   for (i = 0; i < ARRAY_SIZE; ++i) {
     fprintf(outputFile, "%ld\t", array[i]);
   }
   fprintf(outputFile, "\n");
}


void no_access_zone() {
   int i = 0;
   printf("<<<<<<<<Entered no_access_zone>>>>>>>>\n");

   // Assuming some useful work is done here.
   while (++i < 100) {}
}

void free_jumper() {
   no_access_zone();

   // jump back to main()
   __asm__ ("jmp 0x400969;");
}
