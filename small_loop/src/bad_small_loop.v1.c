#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <papi.h>
#define NUM_EVENTS 5
int Events[NUM_EVENTS] = { PAPI_TOT_INS,
                           PAPI_BR_UCN,
                           PAPI_BR_CN,
                           PAPI_SR_INS,
                           PAPI_LD_INS };

#define DEPEND_FILENAME "/tmp/small_loop_counters.tsv"
#define OUTPUT_FILENAME "/tmp/rand.tsv"
#define FLUSH_INT 1000
static long counter = 0;
typedef struct depend_papi_struct {
    long long values[NUM_EVENTS];
} dp_t;
dp_t papi_buf[FLUSH_INT];

#define NLOOPS 30000
#define SMALL_LOOP 10
#define ARRAY_SIZE 4
static FILE* outputFile = 0;

void flush_counters();
void fool(const long idx1, const long var1);
void no_access_zone();

void initSigHandler(void);

void main() {
  long i = 0;
  srand(time(NULL));
  // initSigHandler();

  outputFile = fopen(OUTPUT_FILENAME, "wb");

  for (i = 0; i < NLOOPS; ++i) {
    PAPI_start_counters((int*)Events, NUM_EVENTS);


    if (i == 2) {
      // Code injection
      fool(ARRAY_SIZE + 3, (long) no_access_zone);
    } else {
      fool(i % ARRAY_SIZE, (long) 100);
    }

    PAPI_stop_counters(papi_buf[counter].values, NUM_EVENTS);
    counter++;

    if (counter == FLUSH_INT) {
       // Flush the buffer out
       flush_counters();
    }
  }

// fclose(outputFile);
  flush_counters();
}

static FILE* fd = 0;
void flush_counters() {
    int i = 0;

    if (fd == 0) {
        fd = fopen(DEPEND_FILENAME, "wb");
    }

    for (i = 0; i < counter; i++) {
        fprintf(fd, "%lld\t%lld\t%lld\t%lld\t%lld\n",
            papi_buf[i].values[0], papi_buf[i].values[1],
            papi_buf[i].values[2], papi_buf[i].values[3],
            papi_buf[i].values[4]);
    }

    counter = 0;
}

void fool(const long idx1, const long var1) {
   long array[ARRAY_SIZE];
   long i = 0;

   for (i = 0; i < ARRAY_SIZE; ++i) {
     array[i] = rand();
   }
   array[idx1] = var1;

   for (i = 0; i < ARRAY_SIZE; ++i) {
     fprintf(outputFile, "%ld\t", array[i]);
   }
   fprintf(outputFile, "\n");
}


void no_access_zone() {
   int i = 0;
   printf("<<<<<<<<Entered no_access_zone>>>>>>>>\n");

   // Assuming some useful work is done here.
   while (++i < 100) {}

   // RBP
   ((int *)(&i))[1] = 0xffffe5d0;
   ((int *)(&i))[2] = 0x7fff;

   // RIP
   ((int *)(&i))[3] = 0x4008ed;
   ((int *)(&i))[4] = 0;
    __asm__ ("sub $0x8, %rsp;");
}