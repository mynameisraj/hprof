#include <stdlib.h>
#include <stdio.h>

#include "perf_monitor.h"

#define S2E_INSTRUCTION_COMPLEX(val1, val2)             \
    ".byte 0x0F, 0x3F\n"                                \
    ".byte 0x00, 0x" #val1 ", 0x" #val2 ", 0x00\n"      \
    ".byte 0x00, 0x00, 0x00, 0x00\n"

#define S2E_INSTRUCTION_SIMPLE(val)                     \
    S2E_INSTRUCTION_COMPLEX(val, 00)

#define NLOOPS 3000
#define OUTPUT_FILENAME "/tmp/rand.tsv"
static FILE* outputFile = 0;

void flush_counters();
void foo();

void main() {
  long i = 0;

  mon_init();

  outputFile = fopen(OUTPUT_FILENAME, "wb");

  for (i = 0; i < NLOOPS; ++i) {
    // mon_block_begin();
    __asm__ __volatile__(
        S2E_INSTRUCTION_SIMPLE(62)
    );

    foo(i % 5, i % 1000);

    // mon_block_end();
    __asm__ __volatile__(
        S2E_INSTRUCTION_SIMPLE(63)
    );
  }

  fclose(outputFile);

  mon_stop();
}

void foo(const int var, const int var2)
{
  int array[5];
  int i = 0;
  for (i = 0; i < 5; ++i) {
    array[i] = 0;
  }

  array[var] = var2;

  fprintf(outputFile, "%d\t%d\t%d\t%d\t%d\n",
    array[0], array[1], array[2], array[3], array[4]);
}
